package tp5;

//Jframe va nous permettre de g�rer les f�netres graphique
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;



public class MonAppliGraphique extends JFrame{
	//Attributs

	//On cr�e les diff�rents boutons
	JButton b0 = new JButton("Bouton 0");
	JButton b1 = new JButton("Bouton 1");
	JButton b2 = new JButton("Bouton 2");
	JButton b3 = new JButton("Bouton 3");
	JButton b4 = new JButton("Bouton 4");

	//On cr�e un nouveau panel
	Container panel = getContentPane();

	//JLabel monLabel = new JLabel("Je suis un label"); 
	//JTextField monTextField = new JTextField("Je suis un text field");


	//Constructeurs
	public MonAppliGraphique() {
		//On va h�riter des caract�re de JFrame
		super();
		//On met un titre � notre application
		this.setTitle("Mon application");
		//On choisit la dimension de la fen�tre
		this.setSize(600,600);
		//On choisit l'emplacement de la fen�tre � partir du coin en haut � gauche
		this.setLocation(100,200);
		//Cela va permettre de terminer l'application � la fermeture de la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//On rend la fen�tre visible
		this.setVisible(true);

		//On ajoute les diff�rents bouton � la fen�tre
		panel.add(b0);
		panel.add(b1);
		panel.add(b2);
		panel.add(b3);
		panel.add(b4);
		//panel.add(monLabel);
		//panel.add(monTextField);
		//panel.setLayout(new FlowLayout());	

		//3 lignes, 2 colones
		panel.setLayout(new GridLayout(3, 2));
		//panel.add(b1, BorderLayout.NORTH);
		//panel.add(b3, BorderLayout.SOUTH);
		//panel.add(b2, BorderLayout.EAST);
		//panel.add(b4, BorderLayout.WEST);
		//panel.add(b0, BorderLayout.CENTER);

		//On affiche une chaine de caract�re pour confirmer la cr�ation de la f�netre
		System.out.println("La fen�tre est cr�e !");


	}
	//On cr�e le main pour pouvoir executer le programme
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique ();
		CompteurDeClic compt = new CompteurDeClic() ;

	}

}