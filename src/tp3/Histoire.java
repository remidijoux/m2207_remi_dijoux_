package tp3;

public class Histoire{

	public static void main(String[] args) {
		
		System.out.println("\nExercice 1");
		//On cr�e un nouvel objet humain du nom de "Bob"
		Humain h1 = new Humain("Bob");
		///L'objet h1 utilise la m�thode "sePresenter" de la class Humain
		h1.sePresenter();
		///L'objet h1 utilise la m�thode boire de class Humain
		h1.boire();
		
		System.out.println("\nExercice 2");
		//On cr�e un nouvel objet f1 de type Dame qui aura comme nom "Rachel"
		Dame f1 = new Dame("Rachel");
		///L'objet f1 utilise la m�thode "sePresenter" de la class Dame qui a h�rit� de class Humain
		f1.sePresenter();
		///L'objet f1 utilisee la m�thode "prisEnOtage" de la class Dame qui a h�rit� de class Humain
		f1.priseEnOtage();
		///L'objet f1 utilise la m�thode "estLiberee" de la class Dame qui a h�rit� de class Humain
		f1.estLiberee();
		
		System.out.println("\nExercice 3");
		//On cr�e un objet b1 de type Brigand qui aura comme nom "Gunther"
		Brigand b1 = new Brigand("Gunther");
		///L'objet b1 utilise la m�thode "sePresenter" de la class Brigant qui a h�rit� de la class Humain
		b1.sePresenter();
		
		System.out.println("\nExercice 4");
		//On cr�e un nouvel objet c1 de type Cowboy qui aura comme param�tre "Simon" qui d�finera le nom
		Cowboy c1 = new Cowboy("Simon");
		///L'objet c1 utilise la m�thode "sePresenter" de la class Cowboy qui a h�rit� de la class Humain
		c1.sePresenter();
	
		
		System.out.println("\nExercice 7");
		///L'objet f1 utilise la m�thode "sePresenter" de la class Dame qui a h�rit� de la class Humain
		f1.sePresenter();
		///L'objet b1 utilise la m�thode "sePresenter" de la class Brigand qui a h�rit� de la class Humain
		b1.sePresenter();
		///L'objet b1 utilise la m�thode "enleve" de la class Brigand qui a h�rit� de la class Humain. On d�finit l'objet f1 comme param�tre
		b1.enleve(f1);
		///L'objet b1 utilise la m�thode "sePresenter" de la class Brigant qui a h�rit� de la class Humain
		b1.sePresenter();
		///L'objet f1 utilise la m�thode "sePresenter" de la class Brigant qui a h�rit� de la class Humain
		f1.sePresenter();
		///L'objet c1 utilise la m�thode "tire" de la class Cowboy
		c1.tire(b1);
		///L'objet f1 utilise la m�thode "estLiberee" de la class Dame
		f1.estLiberee();
		///L'objet f1 utilise la m�thode "sePresenter" de la class Dame qui a h�rit� de la class Humain
		f1.sePresenter();
		
		System.out.println("\nExercice 8");
		//On cr�e un nouvel objet "s1" de type Sherif qui prendra comme param�tre "Marshall" qui d�finira le nom
		Sherif s1 = new Sherif("Marshall");
		//L'objet s1 utilise la m�thode "sePresenter" de la class Sherif qui a h�rit� de la class Cowboy dont lui m�me � h�rit� de la class Humain
		s1.sePresenter();
		
		System.out.println(" ");
		///L'objet s1 utilise la m�thode "coffrer" de la class Sherif qui a h�rit� de la class Humain. Il prend en param�tre l'objet b1
		s1.coffrer(b1);
		///L'objet b1 utilise la m�thode "coffrer" de la class Brigand qui a h�rit� de la class Humain. Il prend en param�tre l'objet s1
		b1.emprisonner(s1);
		
		System.out.println(" ");
		//L'objet s1 utilise la m�thode "sePresenter" de la class Sherif qui a h�rit� de la class Cowboy dont lui m�me � h�rit� de la class Humain
		s1.sePresenter();
		
		System.out.println("\nExercice 9");
		//On cr�e un nouvel objet client de type Sherif qui h�rite des attributs de la class Cowboy
		Cowboy clint = new Sherif("Clint");
		///L'objet fclint utilise la m�thode "sePresenter" de la class Sherif 
		clint.sePresenter();
		
		System.out.println("\nExercice 10");
		//On cr�e un nouvel objet p1 de type prison et on lui donne le param�tre "Fort Boyard" qui correspondra � son nom
		Prison p1 = new Prison("Fort Boyard");
		///L'objet p1 utilise la m�thode "mettreEnCellule" de la class prison
		p1.mettreEnCellule(b1);
		///L'objet p1 utilise la m�thode "sortirDeCellule" de la class prison
		p1.sortirDeCellule(b1);

		
	}

}
