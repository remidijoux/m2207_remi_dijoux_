package tp3;

public class Brigand extends Humain { 
	
//Attributs
	
	//On d�lcare l'attribut look de type String
	private String look;
	//On d�clare 'attribut nbkidnap de type int
	private int nbkidnap;
	//On d�clare l'attribut reward de type int
	private int reward;
	//On d�clare un attribut prison de type boolean
	private boolean prison;
	
	
//Constructeur
	
	//Ce constructeur va nous permettre de d�finir le nom d'un brigand
	public Brigand(String nom) {
		//super va nous permettre de prendre les attributs de la class humain
		super(nom);
		//On d�finit la valeur de l'attribut "boisson"
		this.boisson="cognac";
		//On initialise la valeur de l'attribut "look" � m�chant
		look="m�chant";
		//On initialise la valeur de l'entier reward � 100
		reward=100;
		//On initialise la valeur de nbkidnap � 0
		nbkidnap=0;
	
//Accesseurs
	}
	//Cet accesseur va nous permettre de retourner la valeur de la r�compense
	public  int getRecompense() {
		return reward;
	}
	//Cet accesseur va nous permmette d'obtenir le nom du brigand 
	public String quelEstTonNom() {
		return nom +" le " +look;
	}
	
//M�thode
	
	//Cette methode va nous permettre d'avoir la pr�sentation du brigand
	public void sePresenter( ) {
		//super va nous permettre de reprendre la methode "sePresenter" de la class Humain
		super.sePresenter();
		//On utilise l'accesseur parler pour �crire une chaine de caract�re. Ceci va nous permettra d'afficher une chaine de caract�re pour le dialogue du brigand
		parler((nom+ " - J'ai l'air " +look+ " et j'ai enlev� " +nbkidnap+ " dames"));
		parler((nom+ " - Ma t�te est mise � prix " +reward+ "$ !!!"));
	}
	
	//Cette methode va nous permettre d'avoir une une chaine de caract�re gr�ce � l'accesseur "parler()". Cette methode nous donnera le nombre de dame kidnap�e
	public void nombrekidnap() {
			parler(("Voici le nombre de kidnap�e " + nbkidnap));
	}
	
	//Cette methode permettra au brigand de kidnapper une dame. Il prendra comme param�tre l'objet qui correspond � la dame kidnapp�e.
	public void enleve(Dame dame) 
	{
			//La r�compense de la capture du brigand augmente de 100 � chaque fois qu'il kidnappe une dame
			reward=reward+100;
			//Ceci va nous permettre d'augmenter le nombre de kidnappage
			nbkidnap++;
			parler((nom+ " - Ah ah ! " +dame.quelEstTonNom()+ " ,tu es ma prisonni�re"));
			//On utilise la methode prise en otage de la class dame pour montrer qu'elle est kidnapp�e
			dame.priseEnOtage();		
	}
	
	//Cette methode va nous permettre d'emprisonner le brigand gr�ce au Sherif
	public void emprisonner(Sherif s) {
		// "s.nom" va nous permmettre d'obtenir le nom du sherif qui a emprisonner le brigand dans la class Sherif
		parler((nom+" - Dammed je suis fait ! "+ s.nom +" ,tu m'as eu !"));
	}

}
